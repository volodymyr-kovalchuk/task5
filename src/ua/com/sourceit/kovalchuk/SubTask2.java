package ua.com.sourceit.kovalchuk;

public class SubTask2 {
    static class ThreadExtendClass extends Thread {
        public void run() {
            printNameTask();
        }
    }

    static Runnable runnable = SubTask2::printNameTask;

    public static void main(String[] args) {
        ThreadExtendClass threadExtendClass = new ThreadExtendClass();
        Thread runnableThread = new Thread(runnable);

        threadExtendClass.start();
        try {
            threadExtendClass.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        System.out.println("A thread created using the Thread class extension has ended\n");

        runnableThread.start();
        try {
            runnableThread.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        System.out.println("A thread created using the implementation of the Runnable interface has ended\n");

        System.out.println("The main thread \"" + Thread.currentThread().getName() + "\" ends its work");
    }

    private static void printNameTask() {
        long startTime = System.currentTimeMillis();
        while (System.currentTimeMillis() < startTime + 2000) {
            System.out.println(Thread.currentThread().getName());
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}