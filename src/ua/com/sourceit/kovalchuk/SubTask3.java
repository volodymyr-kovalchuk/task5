package ua.com.sourceit.kovalchuk;

public class SubTask3 {
    private int counter1 = 1;
    private int counter2 = 1;

    public static void main(String[] args) {
        SubTask3 asynchronousObject = new SubTask3();
        Runnable asynchronousRunnable = () -> asynchronousMainTask(asynchronousObject);
        Thread thread1 = new Thread(asynchronousRunnable);
        Thread thread2 = new Thread(asynchronousRunnable);
        thread1.start();
        thread2.start();
        joinThread(thread1);
        joinThread(thread2);
        System.out.println("Result of asynchronous work\n");

        SubTask3 synchronizedObject = new SubTask3();
        Runnable synchronizedRunnable = () -> synchronizedMainTask(synchronizedObject);
        Thread thread3 = new Thread(synchronizedRunnable);
        Thread thread4 = new Thread(synchronizedRunnable);
        thread3.start();
        thread4.start();
        joinThread(thread3);
        joinThread(thread4);
        System.out.println("Result of synchronized work");
    }

    private static void joinThread(Thread thread1) {
        try {
            thread1.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public int getCounter1() {
        return counter1;
    }

    public int getCounter2() {
        return counter2;
    }

    public void setCounter1(int counter1) {
        this.counter1 = counter1;
    }

    public void setCounter2(int counter2) {
        this.counter2 = counter2;
    }

    private synchronized static void synchronizedMainTask(SubTask3 mainObject) {
        asynchronousMainTask(mainObject);
    }

    private static void asynchronousMainTask(SubTask3 mainObject) {
        for (int i = 0; i < 10; i++) {
            compareCountersAndPrintResult(mainObject);
            incrementFirstCounter(mainObject);
            sleepFor10Millis();
            incrementSecondCounter(mainObject);
        }
    }

    private static void compareCountersAndPrintResult(SubTask3 mainObject) {
        int counter1 = mainObject.getCounter1();
        int counter2 = mainObject.getCounter2();
        if (counter1 > counter2) System.out.println(Thread.currentThread().getName() + ": counter1 > counter2" +
                " (counter1 = " + counter1 + ", counter2 = " + counter2 + ")");
        else if (counter2 > counter1) System.out.println(Thread.currentThread().getName() + ": counter2 > counter1" +
                " (counter1 = " + counter1 + ", counter2 = " + counter2 + ")");
        else System.out.println(Thread.currentThread().getName() + ": counter1 = counter2" +
                    " (counter1 = " + counter1 + ", counter2 = " + counter2 + ")");
    }

    private static void incrementFirstCounter(SubTask3 mainObject) {
        int counter1 = mainObject.getCounter1();
        mainObject.setCounter1(++counter1);
    }

    private static void incrementSecondCounter(SubTask3 mainObject) {
        int counter2 = mainObject.getCounter2();
        mainObject.setCounter2(++counter2);
    }

    private static void sleepFor10Millis() {
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}