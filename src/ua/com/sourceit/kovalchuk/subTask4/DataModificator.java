package ua.com.sourceit.kovalchuk.subTask4;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DataModificator {
    private final Connection connection;

    public DataModificator(Connection connection) {
        this.connection = connection;
    }

    public void insertNewOrderWithTodayGoods() {
        int orderNumber = insertOrderAndGetOrderNumber();
        List<int[]> listOfGoods = getListOfGoods();
        insertGoodsToOrder(orderNumber, listOfGoods);
    }

    private void insertGoodsToOrder(int orderNumber, List<int[]> listOfGoods) {
        for (int[] good : listOfGoods) {
            String insertGoodsToOrderSql = "INSERT INTO public.goods_to_order (id, \"order\", good, quantity)\n" +
                    "VALUES (DEFAULT, ?, ?, ?)";
            try (PreparedStatement preparedStatement = connection.prepareStatement(insertGoodsToOrderSql)) {
                preparedStatement.setInt(1, orderNumber);
                preparedStatement.setInt(2, good[0]);
                preparedStatement.setInt(3, good[1]);
                preparedStatement.executeUpdate();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    private List<int[]> getListOfGoods() {
        List<int[]> goods = new ArrayList<>();
        try (Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery("SELECT gto.good,\n" +
                    "SUM(gto.quantity)  AS \"good_quantity\"\n" +
                    "FROM \"order\" o\n" +
                    "JOIN goods_to_order gto on o.number = gto.\"order\"\n" +
                    "WHERE o.date = CURRENT_DATE\n" +
                    "GROUP BY gto.good");
            while (rs.next()) {
                int goodId = (Integer) rs.getObject(1);
                long goodQuantity = (Long) rs.getObject(2);
                goods.add(new int[]{goodId, (int) goodQuantity});
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
        return goods;
    }

    private int insertOrderAndGetOrderNumber() {
        String insertOrderSql = "INSERT INTO public.\"order\" (number, date)\n" +
                "VALUES (DEFAULT, CURRENT_DATE)";
        try (PreparedStatement preparedStatement = connection.prepareStatement(insertOrderSql, Statement.RETURN_GENERATED_KEYS)) {
            if (preparedStatement.executeUpdate() == 1) {
                ResultSet addedEntry = preparedStatement.getGeneratedKeys();
                addedEntry.next();
                return addedEntry.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }

    public void deleteOrdersWithSpecifiedQuantityOfGood(String goodName, int quantity) {
        String insertGoodsToOrderSql = "DELETE\n" +
                "FROM \"order\" o\n" +
                "WHERE o.number IN (SELECT o.number AS \"order_number\"\n" +
                "FROM \"order\" o\n" +
                "JOIN goods_to_order gto on o.number = gto.\"order\"\n" +
                "JOIN good g on g.id = gto.good\n" +
                "WHERE g.name = ? AND gto.quantity = ?);";
        try (PreparedStatement preparedStatement = connection.prepareStatement(insertGoodsToOrderSql)) {
            preparedStatement.setString(1, goodName);
            preparedStatement.setInt(2, quantity);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }
}
