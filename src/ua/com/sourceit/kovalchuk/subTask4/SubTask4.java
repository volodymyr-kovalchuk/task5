package ua.com.sourceit.kovalchuk.subTask4;

import java.sql.*;

public class SubTask4 {
    public static void main(String[] args) {
        Connection connection = new DBConnector().getConnection();

        DataSelector dataSelector = new DataSelector(connection);
        dataSelector.showOrdersInfoById(1);
        dataSelector.showOrdersFilteredByTotalCostAndGoodsNumber(200, 3);
        dataSelector.showOrdersWithSelectedGood("Water");
        dataSelector.showTodayOrdersWithoutSelectedGood("Candies");

        DataModificator dataModificator = new DataModificator(connection);
        dataModificator.insertNewOrderWithTodayGoods();
        dataModificator.deleteOrdersWithSpecifiedQuantityOfGood("Cookies", 3);

        try {
            connection.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}