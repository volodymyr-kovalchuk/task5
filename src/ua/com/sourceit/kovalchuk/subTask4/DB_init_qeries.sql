create database subtask4;



create table public."order"
(
    number            serial
        constraint order_pk
            primary key,
    date              date    not null
);





create table public."good"
(
    id          serial
        constraint good_pk
            primary key,
    name        varchar not null,
    description varchar,
    price       integer not null
);


create table public."goods_to_order"
(
    id       serial
        constraint goods_to_order_pk
            primary key,
    "order"  integer not null
        constraint goods_to_order_order_number_fk
            references public."order" ON DELETE CASCADE,
    "good"     integer not null
        constraint goods_to_order_good_id_fk
            references public."good",
    quantity integer not null
);




INSERT INTO public.good (id, name, description, price)
VALUES (DEFAULT, 'Sugar', 'Sweet sugar', 20);

INSERT INTO public.good (id, name, description, price)
VALUES (DEFAULT, 'Tea', 'Tea 20 bags', 50);

INSERT INTO public.good (id, name, description, price)
VALUES (DEFAULT, 'Coffee', '250 g', 70);

INSERT INTO public.good (id, name, description, price)
VALUES (DEFAULT, 'Water', '20 liters', 30);

INSERT INTO public.good (id, name, description, price)
VALUES (DEFAULT, 'Candies', 'Roshen', 100);

INSERT INTO public.good (id, name, description, price)
VALUES (DEFAULT, 'Cookies', null, 40);

INSERT INTO public.good (id, name, description, price)
VALUES (DEFAULT, 'Milk', '900 ml', 30);





INSERT INTO public."order" (number, date)
VALUES (DEFAULT, '2023-02-01');

INSERT INTO public."order" (number, date)
VALUES (DEFAULT, '2023-02-07');

INSERT INTO public."order" (number, date)
VALUES (DEFAULT, '2023-02-07');




INSERT INTO public.goods_to_order (id, "order", good, quantity)
VALUES (DEFAULT, 1, 1, 2);

INSERT INTO public.goods_to_order (id, "order", good, quantity)
VALUES (DEFAULT, 1, 3, 3);

INSERT INTO public.goods_to_order (id, "order", good, quantity)
VALUES (DEFAULT, 1, 4, 1);

INSERT INTO public.goods_to_order (id, "order", good, quantity)
VALUES (DEFAULT, 1, 7, 1);

INSERT INTO public.goods_to_order (id, "order", good, quantity)
VALUES (DEFAULT, 2, 2, 2);

INSERT INTO public.goods_to_order (id, "order", good, quantity)
VALUES (DEFAULT, 2, 6, 1);

INSERT INTO public.goods_to_order (id, "order", good, quantity)
VALUES (DEFAULT, 2, 4, 1);

INSERT INTO public.goods_to_order (id, "order", good, quantity)
VALUES (DEFAULT, 3, 5, 2);

INSERT INTO public.goods_to_order (id, "order", good, quantity)
VALUES (DEFAULT, 3, 6, 3);