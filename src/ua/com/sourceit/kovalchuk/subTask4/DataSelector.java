package ua.com.sourceit.kovalchuk.subTask4;

import java.sql.*;

public class DataSelector {
    private final Connection connection;

    public DataSelector(Connection connection) {
        this.connection = connection;
    }

    public void showOrdersInfoById(int orderNumber) {
        String sql = "SELECT o.number AS \"order_number\", g.name AS \"good_name\",\n" +
                "g.price AS \"good_price\", gto.quantity AS \"good_quantity\",\n" +
                "g.description AS \"good_description\", o.date AS \"order_date\"\n" +
                "FROM \"order\" o\n" +
                "JOIN goods_to_order gto on o.number = gto.\"order\"\n" +
                "JOIN good g on g.id = gto.good\n" +
                "WHERE number = ?";
        try (PreparedStatement stmt = connection.prepareStatement(sql)
        ) {
            stmt.setInt(1, orderNumber);
            ResultSet rs = stmt.executeQuery();
            showInfo(rs);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void showOrdersWithSelectedGood(String goodName) {
        String sql = "SELECT o.number AS \"order_number\"" +
                "FROM \"order\" o\n" +
                "JOIN goods_to_order gto on o.number = gto.\"order\"\n" +
                "JOIN good g on g.id = gto.good\n" +
                "WHERE g.name = ?";
        try (PreparedStatement stmt = connection.prepareStatement(sql)
        ) {
            stmt.setString(1, goodName);
            ResultSet rs = stmt.executeQuery();
            showInfo(rs);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void showOrdersFilteredByTotalCostAndGoodsNumber(int maxOrderCost, int goodsQuantityInOrder) {
        String sql = "SELECT o.number AS \"order_number\", SUM(g.price * gto.quantity) AS \"order_cost\",\n" +
                "COUNT(g.id) AS \"goods_in_order\"\n" +
                "FROM \"order\" o\n" +
                "JOIN goods_to_order gto on o.number = gto.\"order\"\n" +
                "JOIN good g on g.id = gto.good\n" +
                "GROUP BY o.number\n" +
                "HAVING SUM(g.price * gto.quantity) < ? AND COUNT(g.id) = ?";
        try (PreparedStatement stmt = connection.prepareStatement(sql)
        ) {
            stmt.setInt(1, maxOrderCost);
            stmt.setInt(2, goodsQuantityInOrder);
            ResultSet rs = stmt.executeQuery();
            showInfo(rs);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void showTodayOrdersWithoutSelectedGood(String exceptedGoodName) {
        String sql = "SELECT o.number AS \"order_number\"\n" +
                "FROM \"order\" o\n" +
                "WHERE o.number NOT IN (SELECT o.number AS \"order_number\"\n" +
                "                       FROM \"order\" o\n" +
                "                                JOIN goods_to_order gto on o.number = gto.\"order\"\n" +
                "                                JOIN good g on g.id = gto.good\n" +
                "                       WHERE g.name = ?)\n" +
                "  AND o.date = CURRENT_DATE;";
        try (PreparedStatement stmt = connection.prepareStatement(sql)
        ) {
            stmt.setString(1, exceptedGoodName);
            ResultSet rs = stmt.executeQuery();
            showInfo(rs);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private static void showInfo(ResultSet rs) throws SQLException {
        int entryCounter = 1;
        if (rs.next()) {
            do {
                System.out.println(entryCounter + " entry:");
                int numColumns = rs.getMetaData().getColumnCount();
                for (int i = 1; i <= numColumns; i++) {
                    System.out.println(rs.getMetaData().getColumnLabel(i) + " = " + rs.getObject(i));
                }
                System.out.println();
                entryCounter++;
            } while (rs.next());
        } else System.out.println("There are no results matching your query!");
    }
}
