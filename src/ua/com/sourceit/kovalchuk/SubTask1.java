package ua.com.sourceit.kovalchuk;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class SubTask1 {

    public static final String INPUT_FILE = "src/ua/com/sourceit/kovalchuk/data.txt";
    public static final String OUTPUT_FILE = "src/ua/com/sourceit/kovalchuk/data_sorted.txt";
    public static final int RANDOM_NUMBERS_AMOUNT = 20;
    public static final int MAX_RANDOM_NUMBER = 50;

    public static void main(String[] args) {
        writeToFileRandomNumbers();
        List<Integer> inputNumbers = readFromFile();
        System.out.println("input ==> " + inputNumbers);
        List<Integer> outputNumbers = sortWithBubbleMethod(inputNumbers);
        writeToFileSortedNumbers(outputNumbers);
        System.out.println("output ==> " + outputNumbers);
    }

    private static void writeToFileRandomNumbers() {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(INPUT_FILE))) {
            Random random = new Random();
            StringJoiner stringJoiner = new StringJoiner(" ");
            for (int i = 0; i < RANDOM_NUMBERS_AMOUNT; i++) {
                int number = random.nextInt(MAX_RANDOM_NUMBER + 1);
                stringJoiner.add(String.valueOf(number));
            }
            bw.write(stringJoiner.toString());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void writeToFileSortedNumbers(List<Integer> randomNumbers) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(OUTPUT_FILE))) {
            StringJoiner stringJoiner = new StringJoiner(" ");
            randomNumbers.forEach(e -> stringJoiner.add(String.valueOf(e)));
            bw.write(stringJoiner.toString());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static List<Integer> readFromFile() {
        List<Integer> randomNumbers = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(INPUT_FILE))) {
            while (true) {
                String currentLine = br.readLine();
                if (currentLine == null) break;
                String[] numbers = currentLine.split(" ");
                randomNumbers = Arrays.stream(numbers)
                        .map(Integer::parseInt)
                        .collect(Collectors.toList());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return randomNumbers;
    }

    public static List<Integer> sortWithBubbleMethod(List<Integer> sortedList) {
        for (int i = 0; i < sortedList.size(); i++) {
            boolean isSorted = true;
            for (int j = 1; j < sortedList.size() - i; j++) {
                if (needToSwap(sortedList.get(j - 1), sortedList.get(j))) {
                    int temp = sortedList.get(j - 1);
                    sortedList.set(j - 1, sortedList.get(j));
                    sortedList.set(j, temp);
                    isSorted = false;
                }
            }
            if (isSorted) break;
        }
        return sortedList;
    }

    private static boolean needToSwap(int first, int second) {
        return first > second;
    }
}